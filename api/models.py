from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey, Date
from sqlalchemy.orm import relationship

from api.database import Base


class Car(Base):
    __tablename__ = 'tb_car'

    no = Column(Integer, primary_key=True)
    carno = Column(String(10), unique=True)
    owner = Column(String(20))
    brand = Column(String(20))

    records = relationship('Record', backref='car')


class Record(Base):
    __tablename__ = 'tb_record'

    no = Column(Integer, primary_key=True)
    reason = Column(String(200))
    punish = Column(String(200))
    makedate = Column(Date)
    dealt = Column(Boolean, default=False)
    car_id = Column(Integer, ForeignKey('tb_car.no'))
    is_deleted = Column(Boolean, default=False)
    deleted_time = Column(DateTime)
    updated_time = Column(DateTime)
